#/bin/bash
sudo /etc/init.d/network-manager restart
sudo ip addr del 192.168.2.2/24 dev enp60s0
sudo route del default gw 192.168.2.1 
sudo ip addr flush dev enp60s0
sudo dhclient -v enp60s0