from dronekit import connect, VehicleMode
import time, math
from pymavlink import mavutil
import thread
import pprint
def condition_yaw(vehicle, heading, current_heading, relative=False):

    direction = 1
    if abs(current_heading - heading) > 180:
        direction = -1

    if relative:
        is_relative = 1 #yaw relative to direction of travel
    else:
        is_relative = 0 #yaw is an absolute angle
    # create the CONDITION_YAW command using command_long_encode()
    msg = vehicle.message_factory.command_long_encode(
        0, 0,    # target system, target component
        mavutil.mavlink.MAV_CMD_CONDITION_YAW, #command
        0, #confirmation
        heading,    # param 1, yaw in degrees
        3,          # param 2, yaw speed deg/s
        direction,          # param 3, direction -1 ccw, 1 cw
        is_relative, # param 4, relative offset 1, absolute angle 0
        0, 0, 0)    # param 5 ~ 7 not used
    # send command to vehicle
    vehicle.send_mavlink(msg)

def wait_until_value_reaches_boundary(vehicle, val_lambda, target, error): 
    hdg = getattr(vehicle, val_lambda)
    print ("Waiting for correct heading: curr = %s, target_area = [%d .. %d]" % (hdg, target-error, target+error))
    while True:
        hdg = getattr(vehicle, val_lambda)
        time.sleep(0.1)
        if hdg > target-error and hdg<target+error:
            print ("Reached acceptable heading: curr = %s, target_area = [%d .. %d]" % (hdg, target-error, target+error))
            break

def heading_and_wait_error_5(vehicle, heading):
    condition_yaw(vehicle, heading, vehicle.heading)
    max_error = 5
    wait_until_value_reaches_boundary(vehicle, 'heading', heading, max_error)

