from dronekit import connect, VehicleMode
import time, math
from pymavlink import mavutil
import thread
import pprint

### THIS WILL BE THE NEW LOGIC, CURRENTLY NOT IN USE DUE TO STOCK PX4 FIRMWARE

def condition_yaw(heading, current_heading, relative=False):

    direction = 1
    if abs(current_heading - heading) > 180:
        direction = -1

    if relative:
        is_relative = 1 #yaw relative to direction of travel
    else:
        is_relative = 0 #yaw is an absolute angle
    # create the CONDITION_YAW command using command_long_encode()
    msg = vehicle.message_factory.command_long_encode(
        0, 0,    # target system, target component
        mavutil.mavlink.MAV_CMD_CONDITION_YAW, #command
        0, #confirmation
        heading,    # param 1, yaw in degrees
        3,          # param 2, yaw speed deg/s
        direction,          # param 3, direction -1 ccw, 1 cw
        is_relative, # param 4, relative offset 1, absolute angle 0
        0, 0, 0)    # param 5 ~ 7 not used
    # send command to vehicle
    vehicle.send_mavlink(msg)

def wait_until_value_reaches_boundary(val_lambda, target, error): 
    hdg = getattr(vehicle, val_lambda)
    print ("Waiting for correct heading: curr = %s, target_area = [%d .. %d]" % (hdg, target-error, target+error))
    while True:
        hdg = getattr(vehicle, val_lambda)
        time.sleep(0.1)

        print ("Waiting for correct heading: curr = %s, target_area = [%d .. %d]" % (hdg, target-error, target+error))
        if hdg > target-error and hdg<target+error:
            print ("Reached acceptable heading: curr = %s, target_area = [%d .. %d]" % (hdg, target-error, target+error))
            break

def heading_and_wait_error_5(heading):
    condition_yaw(heading, vehicle.heading)
    max_error = 5
    wait_until_value_reaches_boundary('heading', heading, max_error)

### END OF: THIS WILL BE THE NEW LOGIC, CURRENTLY NOT IN USE DUE TO STOCK PX4 FIRMWARE




### HOPEFULLY WE CAN DITCH THIS ONCE WE CAN FLASH THE BLUEROV OURSELVES
last_desired_heading = 0
last_known_heading = 0
last_desired_depth = 0
last_known_depth = 0
heading_active = False
heading_thread = None

def burst_logic(abs_error, taktgeber):
        clear_out = False
        if abs_error<2:
            clear_out = True
        elif abs_error < 10:
            if taktgeber == 0:
                clear_out = True
            elif taktgeber == 1:
                clear_out = True
            elif taktgeber == 2:
                clear_out = True
            elif taktgeber == 3:
                clear_out = True
            elif taktgeber == 4:
                clear_out = True
            elif taktgeber == 5:
                clear_out = True
            elif taktgeber == 6:
                clear_out = True
            elif taktgeber == 7:
                clear_out = False
            elif taktgeber == 8:
                clear_out = False
            elif taktgeber == 9:
                clear_out = False
        elif abs_error < 15:
            if taktgeber == 0:
                clear_out = True
            elif taktgeber == 1:
                clear_out = True
            elif taktgeber == 2:
                clear_out = True
            elif taktgeber == 3:
                clear_out = True
            elif taktgeber == 4:
                clear_out = True
            elif taktgeber == 5:
                clear_out = False
            elif taktgeber == 6:
                clear_out = False
            elif taktgeber == 7:
                clear_out = False
            elif taktgeber == 8:
                clear_out = False
            elif taktgeber == 9:
                clear_out = False
        elif abs_error < 25:
            if taktgeber == 0:
                clear_out = True
            elif taktgeber == 1:
                clear_out = True
            elif taktgeber == 2:
                clear_out = True
            elif taktgeber == 3:
                clear_out = True
            elif taktgeber == 4:
                clear_out = True
            elif taktgeber == 5:
                clear_out = False
            elif taktgeber == 6:
                clear_out = False
            elif taktgeber == 7:
                clear_out = False
            elif taktgeber == 8:
                clear_out = False
            elif taktgeber == 9:
                clear_out = False
        elif abs_error < 35:
            if taktgeber == 0:
                clear_out = True
            elif taktgeber == 1:
                clear_out = True
            elif taktgeber == 2:
                clear_out = True
            elif taktgeber == 3:
                clear_out = False
            elif taktgeber == 4:
                clear_out = False
            elif taktgeber == 5:
                clear_out = False
            elif taktgeber == 6:
                clear_out = False
            elif taktgeber == 7:
                clear_out = False
            elif taktgeber == 8:
                clear_out = False
            elif taktgeber == 9:
                clear_out = False
        else:
            clear_out = False
        return clear_out

def thread_pid_heading(name, delay):
    global last_desired_heading
    global last_known_heading
    global last_desired_depth
    global last_known_depth
    taktgeber = 0
    while True:
        taktgeber = taktgeber + 1
        taktgeber = taktgeber % 10
        time.sleep(delay)
        hdg = getattr(vehicle, 'heading')
        last_known_heading = hdg

        alt = 0 #getattr(vehicle, 'down')
        last_known_depth = alt
        

        if last_desired_heading is None or hdg is None:
            pass
        else:
            a = last_desired_heading - hdg
            a = (a + 180) % 360 - 180 # signed difference
            abs_error = abs(a)

            # now stupid logic, do in "bursts" depending on error
            clear_out = burst_logic(abs_error, taktgeber)

            if a < 0:
                if clear_out:
                    vehicle.channels.overrides['4'] = 1500
                else:
                    vehicle.channels.overrides['4'] = 1440
            elif a > 0:
                if clear_out:
                    vehicle.channels.overrides['4'] = 1500
                else:
                    vehicle.channels.overrides['4'] = 1560

        print ("changed heading: curr = %s, target = [%d], THR = %d" % (hdg, last_desired_heading,vehicle.channels.overrides['4']))

        if last_desired_depth is None or alt is None:
            pass
        else:
            pass#print "depth",last_known_depth    

def heading_pid(heading):
    global heading_active
    global last_desired_heading
    global last_known_heading

    hdg = getattr(vehicle, 'heading')
    if heading == None:
        last_desired_heading = hdg
    else:
        last_desired_heading = heading
    
    if heading_active == False:
        thread.start_new_thread( thread_pid_heading, ("Thread-1", 0.1, ) )
        heading_active = True

    last_known_heading = hdg
    print ("changed heading: curr = %s, target = [%d]" % (hdg, last_desired_heading))

def heading_wait(error):
    global last_known_heading
    global last_desired_heading
    print ("Waiting for correct heading: curr = %s, target_area = [%d .. %d]" % (last_known_heading, last_desired_heading-error, last_desired_heading+error))
    while True:
        time.sleep(0.1)
        if last_known_heading > last_desired_heading-error and last_known_heading<last_desired_heading+error:
            print ("Reached acceptable heading: curr = %s, target_area = [%d .. %d]" % (last_known_heading, last_desired_heading-error, last_desired_heading+error))
            break
### END: HOPEFULLY WE CAN DITCH THIS ONCE WE CAN FLASH THE BLUEROV OURSELVES

def dump_channels():
    # Get all original channel values (before override)
    print("Channel values from RC Tx:", vehicle.channels)
    # Access channels individually
    print("Read channels individually:")
    print(" Ch1: %s" % vehicle.channels['1'])
    print(" Ch2: %s" % vehicle.channels['2'])
    print(" Ch3: %s" % vehicle.channels['3'])
    print(" Ch4: %s" % vehicle.channels['4'])
    print(" Ch5: %s" % vehicle.channels['5'])
    print(" Ch6: %s" % vehicle.channels['6'])
    print(" Ch7: %s" % vehicle.channels['7'])
    print(" Ch8: %s" % vehicle.channels['8'])
    print("Number of channels: %s" % len(vehicle.channels))

# config
connection_string = '0.0.0.0:14550'
# Connect to UDP endpoint.
vehicle = connect(connection_string, wait_ready=False)


# Basic info
print "Autopilot Firmware version: %s" % vehicle.version


print("Basic pre-arm checks")
# Don't try to arm until autopilot is ready
while not vehicle.is_armable_without_gps:
    print(" Waiting for vehicle to initialise...")
    time.sleep(1)

vehicle.channels.overrides['1'] = 1500
vehicle.channels.overrides['2'] = 1500
vehicle.channels.overrides['3'] = 1500
vehicle.channels.overrides['4'] = 1500
vehicle.channels.overrides['5'] = 1500
vehicle.channels.overrides['6'] = 1500
vehicle.channels.overrides['7'] = 1500
vehicle.channels.overrides['8'] = 1500


vehicle.mode = VehicleMode("ALT_HOLD")
while not vehicle.mode.name=='ALT_HOLD':  #Wait until mode has changed
    print " Waiting for mode change ..."
    time.sleep(1)

print("Arming motors")
vehicle.armed = True

# Confirm vehicle armed before attempting to take off
while not vehicle.armed:
    print(" Waiting for arming...")
    time.sleep(1)


dump_channels()

##############################################
##### MISSION PLANNING LOGIC BEGINS HERE #####
##############################################


rad = 0

i=0
while True:
    heading_pid((0+rad) % 360)
    heading_wait(5) # error of desired value = 5deg, we can start going - PID will go the rest 5deg anyway
    vehicle.channels.overrides['5']=1600
    time.sleep(6)
    vehicle.channels.overrides['5']=1500
    heading_pid((180+rad) % 360)
    heading_wait(5) # error of desired value = 5deg, we can start going - PID will go the rest 5deg anyway
    vehicle.channels.overrides['5']=1600
    time.sleep(6)
    vehicle.channels.overrides['5']=1500
    rad=rad+90
    i=i+1



#########################################
##### END OF MISSION PLANNING LOGIC #####
#########################################


# Disarm vehicle
vehicle.armed = False
time.sleep(1)

# Close vehicle object before exiting script
vehicle.close()
time.sleep(1)