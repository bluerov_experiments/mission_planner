from dronekit import connect, VehicleMode
import time, math
from pymavlink import mavutil
import thread
import pprint

def setmode(vehicle, mode):
	vehicle.mode = VehicleMode(mode)
	while not vehicle.mode.name==mode:  #Wait until mode has changed
	    print " Waiting for mode change ..."
	    time.sleep(1)

def arm(vehicle):
	print("Basic pre-arm checks")
	# Don't try to arm until autopilot is ready
	while not vehicle.is_armable_without_gps:
	    print(" Waiting for vehicle to initialise...")
	    time.sleep(0.1)

	# Basic info
	print "Autopilot Firmware version: %s" % vehicle.version

	print("Arming motors")
	vehicle.armed = True

	# Confirm vehicle armed before attempting to take off
	while not vehicle.armed:
	    print(" Waiting for arming...")
	    time.sleep(0.11)

def unarm(vehicle):
	vehicle.armed=False

def return_armed_vehicle(connection_string, mode):
	# config
	connection_string = '0.0.0.0:14550'
	# Connect to UDP endpoint.
	vehicle = connect(connection_string, wait_ready=False)

	setmode(vehicle, mode)
	arm(vehicle)
	return vehicle

def return_vehicle(connection_string, mode):
	# config
	connection_string = '0.0.0.0:14550'
	# Connect to UDP endpoint.
	vehicle = connect(connection_string, wait_ready=False)
	return vehicle

def cleanup(vehicle):
	unarm(vehicle)
	time.sleep(1)
	vehicle.close()
	time.sleep(0.1)