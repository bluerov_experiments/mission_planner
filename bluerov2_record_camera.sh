#!/bin/bash
DATE=$(date +"%Y%m%d%H%M")
gst-launch-1.0 -e -v udpsrc port=5600 ! application/x-rtp, payload=96 ! rtpjitterbuffer ! rtph264depay ! avdec_h264 ! x264enc ! mp4mux ! filesink location=bluerov_recording_$DATE.mp4
