from dronekit import connect, VehicleMode
import time, math
from pymavlink import mavutil
import thread
import pprint

from controlling_new_firmware import *
from arming import *

connection_string = '0.0.0.0:14550'
vehicle = return_armed_vehicle(connection_string, "GUIDED_NOGPS")


for p in range(9):
    heading_and_wait_error_5(vehicle, 222)

    # Loop forever
    for i in range(30):
        time.sleep(0.1)

    heading_and_wait_error_5(vehicle, 70)

    # Loop forever
    for i in range(30):
        time.sleep(0.1)


cleanup(vehicle)