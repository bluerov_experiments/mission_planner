from dronekit import connect, VehicleMode
import time, math
from pymavlink import mavutil
import thread
import pprint
import struct
import socket
UDP_IP = "192.168.1.7"
UDP_PORT = 10400

sock = socket.socket(socket.AF_INET, # Internet
             socket.SOCK_DGRAM) # UDP

def send_udp_update(x,y,z,rot):
    buf = bytes()
    buf += struct.pack('f', x)
    buf += struct.pack('f', y)
    buf += struct.pack('f', z)
    buf += struct.pack('f', rot)

    sock.sendto(buf, (UDP_IP, UDP_PORT))

from controlling_new_firmware import *
from arming import *

connection_string = '0.0.0.0:14550'
vehicle = return_vehicle(connection_string, "GUIDED_NOGPS")


while True:
	print(vehicle.position)


cleanup(vehicle)